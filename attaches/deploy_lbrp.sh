#!/bin/bash

source "lib.trap.sh"


mail_domain="callpal.com"
my_email="admin@callpal.com"
if [[ `uname -m` =~ 'x86_64' ]]; then
    sys64bit="True" #64位系统
else
    sys64bit=""
fi
if [ `groupmems -g postdrop` != "" ]; then
    groupadd -f postdrop
fi
if [[ `id -un postfix` != "postfix" ]]; then
    useradd -M -g postdrop -s /bin/false -p* postfix
fi

yum install -y openssl libtool db4-devel libsnmp-devel
yum install -y perl-CPAN gmp-devel
yum install -y git gawk sed vim wget expect telnet
yum remove -y postfix

function download()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    if [ ! -d "redis-leveldb" ]; then
        git clone git://github.com/appwilldev/redis-leveldb.git
        cd redis-leveldb
        git submodule init
        git submodule update
        cd ..
    fi
    if [ ! -f "redis-2.6.13.tar.gz" ]; then
        wget http://redis.googlecode.com/files/redis-2.6.13.tar.gz
    fi
    if [ ! -f "phpredis-2.2.3.tar.gz" ]; then
        wget -O phpredis-2.2.3.tar.gz https://github.com/nicolasff/phpredis/archive/2.2.3.tar.gz
    fi
    if [ ! -f "leveldb-1.9.0.tar.gz" ]; then
        wget http://leveldb.googlecode.com/files/leveldb-1.9.0.tar.gz
    fi
    if [ ! -f "libev-4.15.tar.gz" ]; then
        wget http://dist.schmorp.de/libev/libev-4.15.tar.gz
    fi
    if [ ! -f "snappy-1.1.0.tar.gz" ]; then
        wget https://snappy.googlecode.com/files/snappy-1.1.0.tar.gz
    fi    
    if [ ! -f "beanstalkd-1.9.tar.gz" ]; then
        wget -O beanstalkd-1.9.tar.gz https://github.com/kr/beanstalkd/archive/v1.9.tar.gz
    fi
    if [ ! -f "libbeanstalkclient.tar.gz" ]; then
        wget -O libbeanstalkclient.tar.gz https://github.com/bergundy/libbeanstalkclient/tarball/master --no-check-certificate
    fi
    if [ ! -f "php-beanstalk.tar.gz" ]; then
        wget -O php-beanstalk.tar.gz https://github.com/nil-zhang/php-beanstalk/tarball/master --no-check-certificate
    fi
    if [ ! -f "cyrus-sasl-2.1.26.tar.gz" ]; then
        wget ftp://ftp.cyrusimap.org/cyrus-sasl/cyrus-sasl-2.1.26.tar.gz
    fi
    if [ ! -f "cyrus-imapd-2.4.17.tar.gz" ]; then
        wget ftp://ftp.cyrusimap.org/cyrus-imapd/cyrus-imapd-2.4.17.tar.gz
    fi
    if [ ! -f "postfix-2.10.0.tar.gz" ]; then
        wget ftp://ftp.cuhk.edu.hk/pub/packages/mail-server/postfix/official/postfix-2.10.0.tar.gz
    fi
}


function ins_redis()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    rm -rf redis-2.6.13
    tar xzf redis-2.6.13.tar.gz
    cd redis-2.6.13
    make
    #make test 在v2.6.12版本root用户下存在bug
    if [ `whoami` != "root" ]; then
        make test
    fi
    make PREFIX=/opt/redis-2.6.13 install
    cd ..

    #复制配置
    mkdir /opt/redis-2.6.13/etc/
    cp redis-2.6.13/redis.conf /opt/redis-2.6.13/etc/
    cp redis-2.6.13/sentinel.conf /opt/redis-2.6.13/etc/
    #修改配置
    sed -i '/^daemonize/c#daemonize no\ndaemonize yes' /opt/redis-2.6.13/etc/redis.conf
    sed -i '/^loglevel notice/c#loglevel notice\nloglevel warning' /opt/redis-2.6.13/etc/redis.conf
    sed -i '/^logfile stdout/c#logfile stdout\nlogfile /var/log/redis.log' /opt/redis-2.6.13/etc/redis.conf
    #建立软连接
    ln -sf /opt/redis-2.6.13/bin/redis-server /usr/bin/redis-server
    ln -sf /opt/redis-2.6.13/bin/redis-cli /usr/bin/redis-cli
}

function ins_php_redis()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    rm -rf phpredis-2.2.3
    tar xzf phpredis-2.2.3.tar.gz
    cd phpredis-2.2.3
    phpize
    ./configure
    make && make install
    cd ..
    #添加到php.ini
    sed -i '1a\extension=redis.so' /opt/php-5.4.15/etc/php.ini
}


function ins_snappy()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    rm -rf snappy-1.1.0
    tar xzf snappy-1.1.0.tar.gz
    cd snappy-1.1.0
    ./configure
    make && make install
    cd ..
    #运行时需要
    if [ -n "$sys64bit" ]; then
        ln -sf /usr/local/lib/libsnappy.so.1.1.4 /usr/lib64/libsnappy.so.1
    else
        ln -sf /usr/local/lib/libsnappy.so.1.1.4 /usr/lib/libsnappy.so.1
    fi
}


function ins_leveldb()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"    
    tar xzf leveldb-1.9.0.tar.gz -C /opt/
    cd /opt/leveldb-1.9.0
    make
    cd -
    mkdir /opt/leveldb-1.9.0/data
    if [ -n "$sys64bit" ]; then
        ln -sf /opt/leveldb-1.9.0/libleveldb.so.1.9 /usr/lib64/libleveldb.so
    else
        ln -sf /opt/leveldb-1.9.0/libleveldb.so.1.9 /usr/lib/libleveldb.so
    fi
}


function ins_redis_leveldb()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    rm -rf libev-4.15
    tar xzf libev-4.15.tar.gz
    cd libev-4.15
    ./configure --prefix=/opt/libev-4.15
    make && make install
    cd ..
    if [ -n "$sys64bit" ]; then
        ln -sf /opt/libev-4.15/lib/libev.so.4.0.0 /usr/lib64/libev.so.4
    else
        ln -sf /opt/libev-4.15/lib/libev.so.4.0.0 /usr/lib/libev.so.4
    fi

    cd redis-leveldb
    sed -i 's#LIBEV?=/usr#LIBEV?=/opt/libev-4.15#' src/Makefile
    sed -i 's/^LDFLAGS.*$/& -lsnappy/' src/Makefile
    make
    cp redis-leveldb /usr/bin/
    cd ..
}


function ins_beanstalkd()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    rm -rf beanstalkd-1.9
    tar xzf beanstalkd-1.9.tar.gz
    cd beanstalkd-1.9
    sed -i '1cPREFIX=/opt/beanstalkd-1.9' Makefile
    make && make install
    cd ..
    ln -sf /opt/beanstalkd-1.9/bin/beanstalkd /usr/bin/beanstalkd
    mkdir -p /opt/beanstalkd-1.9/wal
}


function ins_beanstalkclient()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    rm -rf bergundy-libbeanstalkclient-*/
    tar xzf libbeanstalkclient.tar.gz
    cd bergundy-libbeanstalkclient-*/
    sed -i '6c./configure --prefix=/opt/libbeanstalkclient' autogen.sh
    ./autogen.sh
    #添加到ld.so.conf
    echo "/opt/libbeanstalkclient/lib" > /etc/ld.so.conf.d/libbeanstalkclient.conf
    ldconfig
    cd ..
}


function ins_php_beanstalkd()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    rm -rf nil-zhang-php-beanstalk-*/
    tar xzf php-beanstalk.tar.gz
    cd nil-zhang-php-beanstalk-*/
    phpize
    ./configure --with-libbeanstalkclient-dir=/opt/libbeanstalkclient
    make && make install
    cd ..
    #添加到php.ini
    sed -i '1aextension=beanstalk.so' /opt/php-5.4.15/etc/php.ini
}


function ins_cyrus_sasl()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    rm -rf cyrus-sasl-2.1.26
    tar xzf cyrus-sasl-2.1.26.tar.gz
    cd cyrus-sasl-2.1.26
    ./configure --prefix=/opt/cyrus/cyrus-sasl-2.1.26 \
            --enable-login --enable-ntlm
    make && make install
    cd ..
}


function ins_cyrus_imapd()
{
    rm -rf cyrus-imapd-2.4.17
    tar xzf cyrus-imapd-2.4.17.tar.gz
    cd cyrus-imapd-2.4.17
    ./configure --prefix=/opt/cyrus/cyrus-imapd-2.4.17 --with-lock=fcntl \
            --with-sasl=/opt/cyrus/cyrus-sasl-2.1.26 \
            --with-openssl
    make && make install
    cd ..
}


function ins_postfix()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    rm -rf /etc/postfix/
    mkdir -p /opt/postfix-2.10.0
    rm -rf postfix-2.10.0
    tar xzf postfix-2.10.0.tar.gz
    cd postfix-2.10.0
    if [ -n "$sys64bit" ]; then
        ln -sf /opt/cyrus/cyrus-sasl-2.1.26/lib/libsasl2.so.3.0.0 /usr/lib64/libsasl2.so.3
    else
        ln -sf /opt/cyrus/cyrus-sasl-2.1.26/lib/libsasl2.so.3.0.0 /usr/lib/libsasl2.so.3
    fi
    make makefiles --always-make \
            AUXLIBS='-L/opt/cyrus/cyrus-sasl-2.1.26/lib/sasl2 -lssl -lcrypto' \
            CCARGS='-I/opt/cyrus/cyrus-sasl-2.1.26/include/sasl -DDEF_COMMAND_DIR=\"/sbin\" -DDEF_CONFIG_DIR=\"/etc/postfix\" -DDEF_DATA_DIR=\"/data\" -DDEF_DAEMON_DIR=\"/libexec\" -DDEF_QUEUE_DIR=\"/spool\" -DFD_SETSIZE=2048 -DUSE_CYRUS_SASL -DUSE_TLS'
    expect << EOD
    spawn make install
    expect "^install_root:"
    send "/opt/postfix-2.10.0\n"
    expect "^tempdir:"
    send "/opt/postfix-2.10.0/temp\n"
    send "\n"
    expect eof
EOD
    #postfix-2.10的问题
    #install_root: [/]                 /opt/postfix-2.10.0
    #tempdir: [/home/xxx/yyy/postfix-2.10.0]   /opt/postfix-2.10.0/temp
    #下面一路回车使用默认值
    #config_directory: [/etc/postfix]
    #command_directory: [/usr/sbin]
    #daemon_directory: [/usr/libexec/postfix]
    #data_directory: [/var/lib/postfix]
    #html_directory: [no]
    #mail_owner: [postfix]
    #mailq_path: [/usr/bin/mailq]
    #manpage_directory: [/usr/local/man]
    #newaliases_path: [/usr/bin/newaliases]
    #queue_directory: [/var/spool/postfix]
    #readme_directory: [no]
    #sendmail_path: [/usr/sbin/sendmail]
    #setgid_group: [postdrop]
    cd ..
    chown -R postfix:postdrop /opt/postfix-2.10.0
}


function ini_postfix()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    ln -sf /opt/postfix-2.10.0/sbin/postfix /usr/sbin/postfix
    ln -sf /opt/postfix-2.10.0/etc/postfix /etc/postfix
    sed -i '/^queue_directory/cqueue_directory = /opt/postfix-2.10.0/spool' /opt/postfix-2.10.0/etc/postfix/main.cf
    sed -i '/^command_directory/ccommand_directory = /opt/postfix-2.10.0/sbin' /opt/postfix-2.10.0/etc/postfix/main.cf
    sed -i '/^daemon_directory/cdaemon_directory = /opt/postfix-2.10.0/libexec' /opt/postfix-2.10.0/etc/postfix/main.cf
    sed -i '/^data_directory/cdata_directory = /opt/postfix-2.10.0/data' /opt/postfix-2.10.0/etc/postfix/main.cf
    cat >> /opt/postfix-2.10.0/etc/postfix/main.cf << EOD
local_recipient_maps =
###############postfix#################################
#以下三行设置MX域名和主域名，请替换成您自己的域名
myhostname = mail.$mail_domain
myorigin = $mail_domain
mydomain = $mail_domain
append_dot_mydomain = no
mydestination = \$myhostname, localhost.\$mydomain, localhost, \$mydomain
#mynetworks = 192.168.1.0/24, 10.0.0.0/24, 127.0.0.0/8
mynetworks = 127.0.0.1, 192.168.0.0/24  #只接收本机和局域网的邮件
#body_checks = regexp:/etc/postfix/body_checks  #用于从邮件中提取信息记录到postfix日志

############################CYRUS-SASL############################
broken_sasl_auth_clients = yes
smtpd_recipient_restrictions=permit_mynetworks,permit_sasl_authenticated,reject_invalid_hostname,reject_non_fqdn_hostname,reject_unknown_sender_domain,reject_non_fqdn_sender,reject_non_fqdn_recipient,reject_unknown_recipient_domain,reject_unauth_pipelining,reject_unauth_destination
smtpd_sasl_auth_enable = yes
smtpd_sasl_local_domain = \$myhostname
smtpd_sasl_security_options = noanonymous
#postfix v2.9以上请注释下面这行，否则启动postfix时弹出警告信息
#smtpd_sasl_application_name = smtpd
smtpd_banner = Welcome to our \$myhostname ESMTP,Warning: Version not Available!
EOD

    cat > /opt/postfix-2.10.0/etc/postfix/smtpd.conf << EOD
echo "pwcheck_method: saslauthd" >> /etc/postfix/smtpd.conf
echo "mech_list: PLAIN LOGIN" >> /etc/postfix/smtpd.conf
EOD

    cat >> /opt/postfix-2.10.0/etc/postfix/body_checks << EOD
/\/account\/([a-z_]+)\/veri/  WARN "\$1"
/verify=3D([0-9a-f]+)/  WARN "\$1"
EOD

    #用postfix代替sendmail
    ln -sf /usr/sbin/sendmail.postfix /usr/sbin/sendmail
    ln -sf /usr/sbin/sendmail.postfix /etc/alternatives/mta
    ln -sf /opt/cyrus/cyrus-sasl-2.1.26/sbin/saslauthd /usr/sbin/saslauthd
    if [ -f "/etc/init.d/sendmail" ]; then
        chkconfig sendmail off
    fi
    chkconfig saslauthd on
    if [ ! -f "/etc/init.d/postfix" ]; then
        rm -f /etc/init.d/postfix
        mkdir -p /etc/noinit/
        printf "y\n"| cp noinit/postfix /etc/noinit/postfix
        ln -sf /etc/noinit/postfix /etc/init.d/postfix
    fi
    chkconfig postfix on
}


function test_postfix()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    /etc/init.d/postfix start
    # 测试，收件人为who@where.com
    telnet localhost 25
    EHLO mail."$mail_domain"
    MAIL FROM: admin@"$mail_domain"
    RCPT TO: "$my_email" NOTIFY=success,failure
    DATA
    subject: Mail test!
    我测试一下postfix是否好使！
    This is just a mail test!!!
    .
    #上面单独一行的点表示消息结束

    #从日志中统计退回、拒收、超时失败的邮件
    #grep "status=bounced" /var/log/maillog | gawk 'match($0,/to=<(.*)>/){print substr($0,RSTART+4,RLENGTH-5)}'
    #grep "status=deferred" /var/log/maillog | gawk 'match($0,/to=<(.*)>/){print substr($0,RSTART+4,RLENGTH-5)}'
    #grep "status=expired" /var/log/maillog | gawk 'match($0,/to=<(.*)>/){print substr($0,RSTART+4,RLENGTH-5)}'
}


tar xzf noinit.tar.gz -C ~ryan/soft/
cd ~ryan/soft/
download
[ -d "/opt/redis-2.6.13" ] || { ins_redis; ins_php_redis; }
#可以不安装leveldb
#[ -d "/opt/leveldb-1.9.0" ] || ins_cyrus_imapd
[ -f "/usr/bin/redis-leveldb" ] || { ins_snappy; ins_redis_leveldb; }
[ -d "/opt/beanstalkd-1.9" ] || { ins_beanstalkd; ins_beanstalkclient; ins_php_beanstalkd; }
[ -d "/opt/cyrus/cyrus-sasl-2.1.26" ] || ins_cyrus_sasl
#可以不安装cyrus-imapd
#[ -d "/opt/cyrus/cyrus-imapd-2.4.17" ] || ins_cyrus_imapd
[ -e "/etc/postfix/main.cf" ] || { ins_postfix; ini_postfix; }


#tar xzf noinit.tar.gz -C /etc/
#cd /etc/noinit/
#find . -type f -name "*.conf" | gawk '{$1=substr($1,3,length($1)-2); system("ln -sf /etc/noinit/" $1 " /etc/init/" $1)}'
#cd -

exit 0

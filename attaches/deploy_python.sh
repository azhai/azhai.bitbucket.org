#!/bin/bash

source "lib.trap.sh"


#wget http://swiftsignal.com/packages/centos/6/x86_64/the-silver-searcher-0.14-1.el6.x86_64.rpm


username=ryan
pyver=`python -c "import sys; print sys.version.split()[0]"`
if [[ `uname -m` =~ 'x86_64' ]]; then
    sys64bit="True" #64位系统
else
    sys64bit=""
fi

pypkgs="mlogging pymssql twisted lxml umysql ujson pillow requests uwsgi bottle scrapy pyquery pytz ipy"

#卸载
#ln -sf /opt/python27/bin/python2.7 /usr/bin/python
#ln -sf /opt/python27/bin/pip-2.7 /usr/bin/pip
#pip list | cut -d ' ' -f 1 | grep -v distribute | grep -v Warning: | xargs pip uninstall -y


yum install -y python-devel python-setuptools
yum install -y libxml2 libxslt
easy_install pip


#在2.6和2.7之间切换
function switch_python()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    if [[ $# -eq 0 ]]; then
        local currver=`python -c "import sys; print sys.version.split()[0]"`
        if [[ "$currver" > "2.7" ]]; then
            tover="2.6"
        else
            tover="2.7"
        fi
    else
        tover=$1
    fi

    rm -f /usr/bin/python /opt/python27/bin/python
    rm -f /usr/bin/python2 /opt/python27/bin/python2
    rm -f /usr/bin/python-config /opt/python27/bin/python-config
    rm -f /usr/bin/easy_install /opt/python27/bin/easy_install
    rm -f /usr/bin/pip /opt/python27/bin/pip

    if [[ "2.6" = "$tover" ]]; then
        ln -sf /usr/bin/python2.6 /usr/bin/python
        ln -sf /usr/bin/python2.6 /usr/bin/python2
        ln -sf /usr/bin/python2.6-config /usr/bin/python-config
        ln -sf /usr/bin/easy_install-2.6 /usr/bin/easy_install
        ln -sf /usr/bin/pip-2.6 /usr/bin/pip
    else
        ln -sf /opt/python27/bin/python2.7 /usr/bin/python
        ln -sf /opt/python27/bin/python2.7 /usr/bin/python2
        ln -sf /opt/python27/bin/python2.7-config /usr/bin/python-config
        ln -sf /opt/python27/bin/easy_install-2.7 /usr/bin/easy_install
        ln -sf /opt/python27/bin/pip-2.7 /usr/bin/pip
    fi
}


function ins_python27()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    if [ ! -f "Python-2.7.5.tgz" ]; then
        wget http://www.python.org/ftp/python/2.7.5/Python-2.7.5.tgz
        wget https://pypi.python.org/packages/source/d/distribute/distribute-0.6.40.tar.gz
    fi
    tar xzf Python-2.7.5.tgz
    cd Python-2.7.5
    ./configure --prefix=/opt/python27
    make && make altinstall
    cd ..
    sed -i '1c#!/usr/bin/python2.6' /usr/bin/yum
    ln -sf /opt/python27/bin/python2.7 /usr/bin/python2.7
    ln -sf /opt/python27/bin/python2.7-config /usr/bin/python2.7-config

    tar xzf distribute-0.6.40.tar.gz
    cd distribute-0.6.40
    python2.7 setup.py install
    cd ..
    ln -sf /opt/python27/bin/easy_install-2.7 /usr/bin/easy_install-2.7
    easy_install-2.7 pip..
    ln -sf /opt/python27/bin/pip-2.7 /usr/bin/pip-2.7
    
}

function ins_virtualenv()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    pip install -q virtualenv virtualenvwrapper
    local bashrcfile="/home/$username/.bashrc"
    echo "" >> $bashrcfile
    echo "export WORKON_HOME=\$HOME/.virtualenvs" >> $bashrcfile
    echo "export PROJECT_HOME=\$HOME/projects" >> $bashrcfile
    #echo "export VIRTUALENVWRAPPER_SCRIPT=/usr/bin/virtualenvwrapper.sh" >> $bashrcfile
    echo "export VIRTUALENVWRAPPER_VIRTUALENV_ARGS='--system-site-packages'" >> $bashrcfile
    echo "source /usr/bin/virtualenvwrapper_lazy.sh" >> $bashrcfile
    echo "" >> $bashrcfile
    #使用示例
    echo "#mkproject -p /opt/python27/bin/python2.7 blog" >> $bashrcfile
    echo "#deactive" >> $bashrcfile
    echo "#workon blog" >> $bashrcfile
    echo "" >> $bashrcfile
    #mkdir -p /home/"$username"/.virtualenvs
    if [ ! -e "/usr/bin/virtualenvwrapper_lazy.sh" ]; then
        ln -sf /opt/python27/bin/virtualenvwrapper_lazy.sh /usr/bin/virtualenvwrapper_lazy.sh
    fi
}


function ins_re2()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    if [ ! -f "re2-20130115.tgz" ]; then
        wget https://re2.googlecode.com/files/re2-20130115.tgz
        tar xzf re2-20130115.tgz
        cd re2
        make && make install
        cd ..
    fi
    pip install -q re2
}


function ins_geoip()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    if [ ! -f "GeoIP-devel-1.4.8-3.el6.x86_64.rpm" ]; then
        yum install -y perl-libwww-perl
        wget http://pkgrepo.linuxtech.net/el6/release/x86_64/GeoIP-1.4.8-3.el6.x86_64.rpm
        wget http://pkgrepo.linuxtech.net/el6/release/x86_64/GeoIP-devel-1.4.8-3.el6.x86_64.rpm
        rpm -ivh GeoIP-1.4.8-3.el6.x86_64.rpm
        rpm -ivh GeoIP-devel-1.4.8-3.el6.x86_64.rpm
    fi
    pip install -q https://github.com/maxmind/geoip-api-python/archive/v1.2.8.tar.gz
}


function ins_packages()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    listpkgs=`pip list | cut -d ' ' -f 1 | tr A-Z a-z`
    for p in $pypkgs
    do
        if [[ ! $listpkgs =~ $p ]]; then
            echo "$p"
            if [[ "mlogging" = "$p" ]]; then
                pip install -q "https://pypi.python.org/packages/source/m/mlogging/mlogging.tar.gz"
            elif [[ "pymssql" = "$p" ]]; then
                if [ ! -f "pymssql-master.zip" ]; then
                    wget -O pymssql-master.zip https://github.com/msabramo/pymssql/archive/master.zip
                fi
                pip install -q pymssql-master.zip
            elif [[ "twisted" = "$p" ]]; then
                if [ ! -f "Twisted-13.0.0.tar.bz2" ]; then
                    wget http://twistedmatrix.com/Releases/Twisted/13.0/Twisted-13.0.0.tar.bz2
                fi
                pip install -q Twisted-13.0.0.tar.bz2
            elif [[ "lxml" = "$p" ]]; then
                if [[ "$pyver" > "2.7" ]]; then
                    if [ ! -f "lxml-3.2.1.tar.gz" ]; then
                        wget https://pypi.python.org/packages/source/l/lxml/lxml-3.2.1.tar.gz
                    fi
                    pip install -q lxml-3.2.1.tar.gz
                else
                    yum install -y python-lxml
                fi
            else
                pip install -q "$p"
            fi
        fi
    done
}


cd ~ryan/soft
if [ ! -f "/opt/python27/bin/python2.7" ]; then
    [[ "$pyver" > "2.7" ]] || ins_python27
    switch_python "2.7"
fi
switch_python "2.6"
ins_packages
[[ -n `pip list | cut -d ' ' -f 1 | grep re2` ]] || ins_re2
#[[ -n `pip list | cut -d ' ' -f 1 | grep geoip` ]] || ins_geoip
[[ -n `pip list | cut -d ' ' -f 1 | grep virtualenv` ]] || ins_virtualenv


exit 0

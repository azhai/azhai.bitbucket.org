#!/bin/bash

source "lib.trap.sh"

rootpass=toor #root密码
username=ryan #用户名
userpass=ryan #用户密码
usersmbpass=ryan  #Samba密码
if [[ `uname -m` =~ 'x86_64' ]]; then
    sys64bit="True" #64位系统
else
    sys64bit=""
fi

yum update -y
yum install -y gcc gcc-c++ make cmake automake autoconf binutils
yum install -y sed gawk vim wget expect
yum install -y ibus-pinyin
yum install -y samba

#修改root密码
echo "root:$rootpass" | chpasswd


groupadd -g 500 "$username"
useradd -r -m -u 500 -g 500 "$username"
echo "$username:$userpass" | chpasswd

gpasswd -a "$username" wheel

wget http://dl.cihar.com/enca/enca-1.14.tar.gz
tar xzf enca-1.14.tar.gz
cd enca-1.14
./configure --prefix=/opt/enca-1.14
make && make install
cd ..
ln -sf /opt/enca-1.14/bin/enca /usr/bin/enca

#自动登录
sed -i "s/^\[daemon\]/& \nAutomaticLoginEnable=true\nAutomaticLogin=$username/" /etc/gdm/custom.conf
#Grub等待时间
sed -i '/^timeout=/ctimeout=0' /boot/grub/grub.conf

#vboxadd
yum install -y kernel-devel
mkdir -p /opt/cdrom
mount /dev/cdrom /opt/cdrom
/opt/cdrom/VBoxLinuxAdditions.run

#samba
(echo "$usersmbpass"; echo "$usersmbpass";) | smbpasswd -a -s "$username"
mv /etc/samba/smb.conf /etc/samba/smb.conf.bak
gawk 'NF>1 && $1!~/^(#|;)/' /etc/samba/smb.conf.bak > /etc/samba/smb.conf
cat > /etc/samba/smb.conf << EOD
[global]
  workgroup = WORKGROUP
  server string = Smaba %V
  hosts allow = 192.168.0.
  security = user
  passdb backend = tdbsam
  log file = /var/log/samba/%m.log
  max log size = 50

[$username]
  path = /home/$username
  browseable = yes
  writable = yes
  valid users = $username
EOD

sed -i 's/^SELINUX=enforcing/# & \nSELINUX=disabled/' /etc/selinux/config
sed -i 's/^SELINUXTYPE=/# &/' /etc/selinux/config
chkconfig iptables off
chkconfig ip6tables off
chkconfig smb on

#sshpass
tar xzf sshpass-1.05.tar.gz
cd sshpass-1.05
./configure
make && make install
cd ..

#mylvmbackup
cd ~ryan/soft/
yum install -y rsync perl-DBI perl-DBD-mysql perl-TimeDate
if [ ! -f "perl-Config-IniFiles-2.72-2.el6.noarch.rpm" ]; then
    wget http://dl.fedoraproject.org/pub/epel/6/i386/perl-Config-IniFiles-2.72-2.el6.noarch.rpm
fi
if [ ! -f "perl-IO-stringy-2.110-10.1.el6.noarch.rpm" ]; then
    wget http://mirror.centos.org/centos/6/os/i386/Packages/perl-IO-stringy-2.110-10.1.el6.noarch.rpm
fi
if [ ! -f "perl-List-MoreUtils-0.22-10.el6.x86_64.rpm" ]; then
    wget http://mirror.centos.org/centos/6/os/x86_64/Packages/perl-List-MoreUtils-0.22-10.el6.x86_64.rpm
fi
rpm -ivh perl-IO-stringy-2.110-10.1.el6.noarch.rpm
rpm -ivh perl-List-MoreUtils-0.22-10.el6.x86_64.rpm
rpm -ivh perl-Config-IniFiles-2.72-2.el6.noarch.rpm
pip install http://web.chad.org/projects/rsnap/rsnap-0.6.tar.gz
if [ ! -f "mylvmbackup-0.13.tar.gz" ]; then
    wget http://www.lenzg.net/mylvmbackup/mylvmbackup-0.13.tar.gz
fi
tar xzf mylvmbackup-0.13.tar.gz
cd mylvmbackup-0.13
sed -i '/prefix =/cprefix = /opt/mylvmbackup-0.13' Makefile
make install
cd ..
ln -sf /opt/mylvmbackup-0.13/bin/mylvmbackup /usr/bin/mylvmbackup

#ntfs-3g
rpm -Uvh http://ftp.jaist.ac.jp/pub/Linux/Fedora/epel/6/x86_64/epel-release-6-8.noarch.rpm
yum install -y ntfs-3g

#vlc player
yum -y localinstall --nogpgcheck http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-7.noarch.rpm
yum -y localinstall --nogpgcheck http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
yum -y localinstall --nogpgcheck http://download1.rpmfusion.org/free/el/updates/6/i386/rpmfusion-free-release-6-1.noarch.rpm
yum -y localinstall --nogpgcheck http://download1.rpmfusion.org/nonfree/el/updates/6/i386/rpmfusion-nonfree-release-6-1.noarch.rpm
yum -y --enablerepo=remi-test info vlc
yum -y --enablerepo=remi-test install vlc

exit 0

#!/bin/bash


#修改mysql管理员密码
#用法 passwd_mysql_root "" toor
function passwd_mysql_root()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    local oldpass=$1
    local rootpass=$2
    mysql -u root --password="$oldpass" mysql << EOD
    DELETE FROM \`user\` WHERE User='';
    DELETE FROM \`db\` WHERE User='';
    SET PASSWORD FOR 'root'@'localhost'=PASSWORD('$rootpass');
    SET PASSWORD FOR 'root'@'127.0.0.1'=PASSWORD('$rootpass');
    SET PASSWORD FOR 'root'@'::1'=PASSWORD('$rootpass');
    SET PASSWORD FOR 'root'@'`hostname`'=PASSWORD('$rootpass');
EOD
}


#增加mysql用户到数据库
#用法 passwd_mysql_user toor dba changeme
function passwd_mysql_user()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    local rootpass=$1
    local dbauser=$2
    local dbapass=$3
    local prefix="db"
    mysql -u root --password="$rootpass" mysql << EOD
    GRANT ALL PRIVILEGES ON \`$prefix\\_%\`.* TO '$dbauser'@'localhost' IDENTIFIED BY '$dbapass' WITH GRANT OPTION;
    GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,ALTER,INDEX ON \`db\\_%\`.* TO '$dbauser'@'192.168.0.%' IDENTIFIED BY '$dbapass' WITH GRANT OPTION;
    FLUSH PRIVILEGES;
    UPDATE user SET File_priv='Y' WHERE User='$dbauser';
    FLUSH PRIVILEGES;
    GRANT FILE ON *.* TO '$dbauser'@'localhost';
    GRANT FILE ON *.* TO '$dbauser'@'192.168.0.%';
    FLUSH PRIVILEGES;
EOD
}


#生成nginx网站配置，需要自己修改server_name
#用法 add_nginx_site example
#用法 add_nginx_site php blog.example.com 80
function add_nginx_site()
{
    echo ""
    echo ""
    echo "$FUNCNAME start ......"
    local sitesdir="/opt/nginx-1.4.1/conf/sites"
    local projectsdir="/home/ryan/projects"
    local template=$1
    local domain="$2" #域名
    local port="$3" #端口，可选
    if [[ -z "$port" ]]; then
        port="80"
    fi
    
    if [[ "example" == "$template" ]]; then
        domain="example"
        cat > $sitesdir/example.conf.bak << EOD
server {
    listen           $port;
    server_name      www.$domain.com;
    charset          utf-8;
    root             $projectsdir/$domain;

    # case insensitive
    #lower \$lower_uri "\$uri";
    #try_files \$uri \$lower_uri;

    set \$var_sessid "-";
    if ( \$http_cookie ~* "PHPSESSID=(\S+)(;.*|\$)")
    {
        set \$var_sessid \$1;
    }
    access_log  logs/$domain.access.log  main;

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    #error_page   500 502 503 504  /50x.html;
    #location = /50x.html {
    #    root   html;
    #}

    if (\$request_uri ~* ^.*\\.(svn|git|hg|bzr|cvs).*\$) {
        return 404;
    }

    location ~* ^.+\\.(css|js|jpg|png|gif|ico|swf|pdf|txt|xlsx)\$ {
        access_log off;
        expires    30d;
    }

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    location / {
        #auth_basic            "Restricted";
        #auth_basic_user_file  authes/$domain;
        try_files \$uri  \$uri/  /index.php?\$query_string;
        #fastcgi_pass   127.0.0.1:9000;
        fastcgi_pass    unix:/var/run/php-fpm/php-fpm.sock;
        fastcgi_index   index.php;
        include         fastcgi_params;
        fastcgi_param   SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
    }

    #location ~* /\$ {
    #    index    index.html;
    #}
}
EOD
    fi
    
    if [[ "media" == "$template" ]]; then
        cat > $sitesdir/media.conf << EOD
server {
    listen           $port;
    server_name      $domain;
    charset          utf-8;
    root             $projectsdir/$domain;

    # case insensitive
    lower \$lower_uri "\$uri";
    try_files \$uri \$lower_uri;
    
    access_log off;
    expires    30d;

    location ~* /\$ {
        index    index.html;
    }
}
EOD
    fi
    
    if [[ "php" == "$template" ]]; then
        cat > $sitesdir/$domain.conf << EOD
server {
    listen           $port;
    server_name      $domain;
    charset          utf-8;
    root             $projectsdir/$domain;

    # case insensitive
    #lower \$lower_uri "\$uri";
    #try_files \$uri \$lower_uri;

    set \$var_sessid "-";
    if ( \$http_cookie ~* "PHPSESSID=(\S+)(;.*|\$)")
    {
        set \$var_sessid \$1;
    }
    access_log  logs/$domain.access.log  main;

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    #error_page   500 502 503 504  /50x.html;
    #location = /50x.html {
    #    root   html;
    #}

    if (\$request_uri ~* ^.*\\.(svn|git|hg|bzr|cvs).*\$) {
        return 404;
    }

    location ~* ^.+\\.(css|js|jpg|png|gif|ico|swf|pdf|txt|xlsx)\$ {
        access_log off;
        expires    30d;
    }

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    location / {
        #auth_basic            "Restricted";
        #auth_basic_user_file  authes/$domain;
        try_files \$uri  \$uri/  /index.php?\$query_string;
        #fastcgi_pass   127.0.0.1:9000;
        fastcgi_pass    unix:/var/run/php-fpm/php-fpm.sock;
        fastcgi_index   index.php;
        include         fastcgi_params;
        fastcgi_param   SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
    }

    #location ~* /\$ {
    #    index    index.html;
    #}
}
EOD
    fi
}

